def find_coins_greedy(amount):
    # Define the coin denominations
    denominations = [50, 10, 2, 1]
    
    # Initialize an empty dictionary to store the result
    result = {}
    
    # Iterate over each denomination
    for coin in denominations:
        # If the amount is greater than or equal to the current coin
        if amount >= coin:
            # Find how many coins of this denomination can be used
            num_coins = amount // coin
            
            # Subtract the value of these coins from the amount
            amount -= num_coins * coin
            
            # Add the number of coins to the result
            result[coin] = num_coins
    
    # Return the result
    return result

# Test the function with print statements
print(find_coins_greedy(113))  # Expected: {50: 2, 10: 1, 2: 1, 1: 1}
print(find_coins_greedy(50))  # Expected: {50: 1}
print(find_coins_greedy(10))  # Expected: {10: 1}
print(find_coins_greedy(2))  # Expected: {2: 1}
print(find_coins_greedy(1))  # Expected: {1: 1}
print(find_coins_greedy(0))  # Expected: {}