def find_min_coins(amount, coins=[1, 2, 5, 10, 20, 50], memo={}):
    # Base case: 0 amount
    if amount == 0:
        return {}

    # Check if result is already in memo
    if amount in memo:
        return memo[amount]

    # Initialize minimum coins and corresponding count
    min_coins = float('inf')
    min_count = None

    # Try each coin
    for coin in coins:
        if amount >= coin:
            count = find_min_coins(amount - coin, coins, memo)
            total_coins = sum(count.values()) + 1
            if total_coins < min_coins:
                min_coins = total_coins
                min_count = count.copy()
                if coin in min_count:
                    min_count[coin] += 1
                else:
                    min_count[coin] = 1

    # Store result in memo
    memo[amount] = min_count

    return min_count

def find_min_coint_wrapper(amount):
    memo = {}
    find_min_coins(amount, memo=memo)
    return memo[amount]

# Test the function with print statements
print(find_min_coint_wrapper(113))  # Expected: {50: 2, 10: 1, 2: 1, 1: 1}
